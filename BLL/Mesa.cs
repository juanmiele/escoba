﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Mesa
    {
        public List<BE.Carta> inicio(BE.Mazo mazo)
        {
            List<BE.Carta> cartas = new List<BE.Carta>();

            for (int m = 0; m < 4; m++)
            {
                cartas.Add(mazo.CARTAS[0]);
                mazo.CARTAS.RemoveAt(0);

            }
            return cartas;
        }
    }
}

