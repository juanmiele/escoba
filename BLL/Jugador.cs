﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugador
    {
        public bool robarDelMazo(BE.Mazo mazo, BE.Jugador jug)
        {
            bool error = false;

            if (mazo.CARTAS.Count() >= 1)
            {
                jug.MANO.CARTAS.Add(mazo.CARTAS[0]);
                mazo.CARTAS.RemoveAt(0);
                jug.MANO.CARTAS = this.ordenarMano(jug.MANO.CARTAS);
            }
            else
            {
                error = true;
            }
            return error;

        }



        private List<BE.Carta> ordenarMano(List<BE.Carta> cartas)
        {
            return cartas.OrderBy(x => x.Palo).ThenBy(x => x.Numero).ToList();

        }



        public List<BE.Carta> inicio(BE.Mazo mazo)
        {
            List<BE.Carta> cartas = new List<BE.Carta>();

            for (int m = 0; m < 3; m++)
            {
                cartas.Add(mazo.CARTAS[0]);
                mazo.CARTAS.RemoveAt(0);

            }
            return cartas;
        }
        //public BE.Jugador inicio(BE.Mazo mazo, BE.Jugador jugador)
        //{

        //    for (int m = 0; m < 3; m++)
        //    {
        //        jugador.MANO.CARTAS.Add(mazo.CARTAS[0]);
        //        mazo.CARTAS.RemoveAt(0);

        //    }
        //    jugador.PUNTOSPARTIDA = 0;
        //    return jugador;
        //}




        private List<BE.Carta> verificarPorNumero(List<BE.Carta> manoAux)
        {
            List<BE.Carta> cartasIguales = new List<BE.Carta>();

            foreach (BE.Carta carta in manoAux)
            {
                var cartasIgualesAux = (from BE.Carta c in manoAux
                                        where c.Numero.Equals(carta.Numero)
                                        select c).ToList();

                if (cartasIgualesAux.Count().Equals(4) || cartasIgualesAux.Count().Equals(3))
                {
                    cartasIguales = cartasIgualesAux;
                    break;
                }

            }

            return cartasIguales;
        }


    }
}
