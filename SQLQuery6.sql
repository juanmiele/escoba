USE [Escoba]
GO

/****** Object:  Table [dbo].[Carta]    Script Date: 7/10/2020 17:54:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Carta](
	[Id] [int] NOT NULL,
	[Numero] [int] NULL,
	[Palo] [int] NULL,
 CONSTRAINT [PK_Carta] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[listarCartas]    Script Date: 7/10/2020 17:54:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[listarCartas]
as 
begin

select * from escoba.dbo.Carta


END
GO


