﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form1 : Form
    {

        BLL.Mazo gestormazo = new BLL.Mazo();
        BLL.Mesa gestomeza = new BLL.Mesa();
        BLL.Jugador gestorjugador = new BLL.Jugador();
        Mazo mazo = new Mazo();
        Mesa mesa = new Mesa();
        Jugador jugador1 = new Jugador();
        Jugador jugador2 = new Jugador();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            inicio();
            actualizar();
        }

        private void inicio()
        {

            mazo.CARTAS = gestormazo.LlenarMazo();
            mesa.CARTAS = gestomeza.inicio(mazo);
            jugador1.MANO.CARTAS = gestorjugador.inicio(mazo);
            jugador2.MANO.CARTAS = gestorjugador.inicio(mazo);


        }

        private void actualizar()
        {


            dataGridView2.DataSource = mesa.CARTAS;
            dataGridView3.DataSource = jugador1.MANO.CARTAS;
            dataGridView4.DataSource = jugador2.MANO.CARTAS;
            dataGridView1.DataSource = mazo.CARTAS;
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            
        }
    }
}
