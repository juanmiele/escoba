﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Carta
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private string palo;
        public string Palo
        {
            get { return palo; }
            set { palo = value; }
        }
    }
}
